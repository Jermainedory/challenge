import json
import csv
from datetime import datetime


class DataAnalyzer(object):
    def __init__(self):
        self.data = []
        self.stats = {}

    def read_csv_file(self, filename):
        with file(filename, 'rU') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.data.append(row)

    def read_json_file(self, filename):
        with file(filename, 'rU') as f:
            json_data = json.load(f)
            for row in json_data['devices']:
                self.data.append(row)

    def store_total_devices(self):
        self.stats['total_devices'] = len(self.data)

    def store_unique_devices(self):
        unique_data = self.get_unique_data()
        self.stats['total_unique_devices'] = len(unique_data)

    def store_oldest_device(self):
        oldest = None
        for row in self.data:
            if oldest is None or row['timestamp'] < oldest['timestamp']:
                oldest = row

        oldest_date = datetime.fromtimestamp(float(oldest['timestamp']))
        self.stats['oldest_device'] = '%s %s' % (oldest['uuid'], oldest_date)

    def store_newest_device(self):
        newest = None
        for row in self.data:
            if newest is None or row['timestamp'] > newest['timestamp']:
                newest = row
        newest_date = datetime.fromtimestamp(float(newest['timestamp']))
        self.stats['newest_device'] = '%s %s' % (newest['uuid'], newest_date)

    def get_unique_data(self):
        unique_data = {}
        for row in self.data:
            if row['uuid'] not in unique_data:
                unique_data[row['uuid']] = row

        return unique_data.values()

    def save_result_file(self, filename):
        with file(filename, 'w+') as f:
            json.dump(self.stats, f)

    def store_devices_in_area(self, coord1, coord2):
        devices = []
        unique_data = self.get_unique_data()

        for row in unique_data:
            if self.is_device_in_area(row, coord1, coord2):
                devices.append(row)

        self.stats['devices_in_area'] = len(devices)

    @staticmethod
    def is_device_in_area(row, coord1, coord2):
        # Sort them to ensure the comparison does -179 <= coordinate <= 0
        lats = sorted((coord1[0], coord2[0]))
        longs = sorted((coord1[1], coord2[1]))
        return (
            row['lat'] >= lats[0] and row['lat'] <= lats[1] and
            row['long'] >= longs[0] and row['long'] <= longs[1]
        )


    def run(self):
        self.store_total_devices()
        self.store_unique_devices()
        self.store_oldest_device()
        self.store_newest_device()
        self.store_devices_in_area((0, 0), (89.99, -179.99))


def main():
    analyzer = DataAnalyzer()
    analyzer.read_csv_file('data.csv')
    analyzer.read_json_file('data.json')
    analyzer.run()
    analyzer.save_result_file('results.json')
    print analyzer.stats

if __name__ == '__main__':
    main()
